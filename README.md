# Mowes Premium III

A wamp localhost server for windows by [CH Software](https://archive.is/iYniS) that is no longer developed.

**Contents**

* Apache 2
* Couch DB
* Dev Kit
* Image Magick
* Java Portable
* Mongo DB
* MySQL
* Perl
* PHP 5
* Python 3.1
* Ruby (can be upgraded)
* Tomcat 6

I've uploaded Mowes Premium III for everyone to download. It has more features than the Mowes Portable II you uploaded.
The 378MB rar file is 1.5GB when extracted.

* [mediafire download link](https://www.mediafire.com/file/o249q7mzejm1km3/mowes+iii+premium.rar/file)
* [box.com part 1](https://app.box.com/s/d9ipe6tpl4hrrl1mp52iiogbrlwq71p4)
* [box.com part 2](https://app.box.com/s/jvt567qngi0rpbiyqk4wh32sagp5tgk1)
